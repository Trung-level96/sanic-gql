# coding: utf-8

import pytest

from app import app as pp_app


@pytest.yield_fixture
def app():
    yield pp_app


@pytest.fixture
def test_cli(loop, app, test_client):
    return loop.run_until_complete(test_client(app))


@pytest.mark.parametrize(('a', 'b', 'exp'), [
    (51.4375, 12.091675, '06184'),
])
async def test_example(a, b, exp):
    print("Hello test")
