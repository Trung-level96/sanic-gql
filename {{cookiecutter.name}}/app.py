# coding: utf-8

import os

from sanic_graphql import GraphQLView
from sanic import Sanic
from api.schema import schema

from graphql.execution.executors.asyncio import AsyncioExecutor


app = Sanic(__name__)

app.config.REQUEST_TIMEOUT = 120
app.config.DEBUG = False if app.config.DEBUG.lower() == 'false' else True
app.debug = app.config.DEBUG

path = '/graphql'


@app.listener('before_server_start')
def init_async_executor(app, loop):
    executor = AsyncioExecutor(loop)
    app.add_route(
        GraphQLView.as_view(schema=schema, executor=executor, graphiql=app.config.DEBUG),
        path
    )


@app.listener('before_server_stop')
def remove_graphql_endpoint(app, loop):
    app.remove_route(path)


if __name__ == '__main__':
    workers = 1 if app.config.DEBUG else os.cpu_count()
    app.run(host='0.0.0.0', debug=app.config.DEBUG, workers=workers)