# {{cookiecutter.full_name}} GraphQL-Backend

## Development with docker

### Requirements
* Docker: 17.06.0
* docker-compose: 1.15.0

### Start Dev-Container
Starting Container with **docker-compose***

```bash
docker-compose run --service-ports api ash
```

OR use shortcut
```bash
./dev start
```

### Start Jupyter Notebook
```bash
jupyter notebook --allow-root --ip=0.0.0.0
```

### Run local develop Server
```bash
python app.py
```

### Run tests
```bash
STAGE=test pytest tests
```
