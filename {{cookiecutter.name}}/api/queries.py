# coding: utf-8

import graphene
import asyncio
import logging

logger = logging.getLogger(__name__)


class ExampleNode(graphene.ObjectType):
    field = graphene.String()

    class Meta:
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    field = graphene.ConnectionField(ExampleNode, name=graphene.String())

    async def resolve_field(self, args, context, info):
        return []
